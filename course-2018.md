## Protein Bioinformatics for Beginners

Feb 26-28, 2018, 9:30 - 16:30
Trainers: Malvika Sharan, Jelena Calyseva, Michael Kuhn, Marc Gouw, Manjeet Kuman, Toby Gibson (order by appearance)

--------------------------------------------------------------------------------------------------
### Important links

- Shared notes: https://piratenpad.de/p/gibson_workshops
- Course Materials: https://git.embl.de/sharan/protein-bioinformatics-embl-hd
- Post workshop survey: https://de.surveymonkey.com/r/denbi-course?sc=hdhub&id=000107

### Suggested schedule

Day|Time|Plan
|----------|----------|----------|
|1|09:30 - 10:15|[Primary databases: Sequence, domains and their annotations](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2018/CourseIntroduction-MalvikaSharan.pdf) - Malvika|
|1|10:15 - 10:30|Break|
|1|10:30 - 11:15|[Studying similar sequences and understanding functional modules in proteins](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2018/Protein_bioinfo-MalvikaSharan.pdf) - Malvika|
|1|11:15 - 11:30|Break|
|1|11:30 - 12:30|Protein structures: Understanding the structure - Jelena|
|1|12:30 - 13:15|Lunch on your own|
|1|13:15 - 14:00|Protein association networks with STRING - 1 - Michael|
|1|14:00 - 14:15|Break|
|1|14:15 - 15:00|Protein association networks with STRING - 2 - Michael|
1|15:00 - 15:15|Break|
|1|15:15 - 16:30|[Understanding linear motifs & intrinsically disordered region](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2018/SLiMs-Slides.pdf) (ELM, SLiMsearch) - Marc|
|----------|----------|----------|
|2|09:30 - 10:15|Marc continues or Manjeet takes over to introduce PTM,  other databases|
|2|10:15 - 10:30|Break|
|2|10:30 - 11:15|Jpred/IUPRED, Anchor - Manjeet|
|2|11:15 - 11:30|Break|
|2|11:30 - 12:30|[Revealing interactive features in protein multiple sequence alignments with Jalview](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/tree/master/TeachingMaterials/2018/MSA_files) - Toby G.|
|2|12:30 - 13:15|Lunch on your own|
|2|13:15 - 14:00|[Visualizing protein structures and interaction interfaces with UCSF Chimera](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/tree/master/TeachingMaterials/2018/Chimera_files) - Toby G.|
|2|14:00 - 14:15|Break|
|2|14:15 - 15:00|[Visualizing protein structures and interaction interfaces with UCSF Chimera](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/tree/master/TeachingMaterials/2018/Chimera_files) - Toby G./Jelena|
|2|15:00 - 15:15|Break|
|2|15:15 - 16:30|Practical activity/Group tasks|
|----------|----------|----------|
|3|9:30 - 12:30|Practical activity/Group tasks: continue|
|----------|----------|----------|

### Group activities: Feb 28

### List of proteins (suggestions):

- P53 -  guardian of the genome and policeman of the oncogenes - Transcription factor - http://www.uniprot.org/uniprot/P04637
- CFTR - Cystic fibrosis susceptibility protein - Transmembrane Ion channel - http://www.uniprot.org/uniprot/P13569
- Hemoglobin -  Oxygen transport - http://www.uniprot.org/uniprot/P68871
- BRCA1 - Breast cancer susceptibility protein - E3 Ubiquitin ligase - http://www.uniprot.org/uniprot/P38398
- Histone - Core component of nucleosome - http://www.uniprot.org/uniprot/P68431
- A-beta - Alzheimer disease amyloid protein - http://www.uniprot.org/uniprot/P05067
- ESR1 - Nuclear hormone receptor for estrogen - http://www.uniprot.org/uniprot/P03372
- SOD1 - Superoxide dismutase, Destroys radicals - http://www.uniprot.org/uniprot/P00441
- Src - Kinase - First oncogene - http://www.uniprot.org/uniprot/P12931
- HIF1 - Hypoxia sensor - http://www.uniprot.org/uniprot/Q16665
- SRY - Sex-determining region Y protein - http://www.uniprot.org/uniprot/Q05066
