# DisProt

DisProt is a collection of manually curated disordered protein regions, and
contains over 800 entries. The DisProt homepage can be found here:
http://www.disprot.org/

## Exercise 1: Browsing DisProt

Navigate to DisProt hompage, and subsequently to the "Browse" section to browse
the database content.

- **Question 1:** How many Rabbit proteins are annotated in DisProt?

Find the DisProt entry for (human) **DNA topoisomerase 1**.

- **Question 2:** How many disorered regious exist in this protein?
- **Question 3:** Which method was used to determine that the region between
  "175 - 214" is disordered?
