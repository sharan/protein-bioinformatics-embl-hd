|**Workshop**|**Protein bioinformatics for beginners**|
|----------|:-------------:|------:|
|**Dates**|8 - 9 November|
|**Time**|09:30 - 17:00 hrs|
|**Venue**|ATC Computer lab, EMBL Heidelberg|
|**Trainers**|Toby Gibson, Marc Gouw, Michael Kuhn, Manjeet Kumar, Benjamin Lang, Malvika Sharan|

## List of resources that will be covered in this workshop

**Part-1 Protein databases and sequence analysis**

1. Protein databases:
    - [Introduction to protein databases](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/ProteinBioinfo-MalvikaSharan.pdf): Malvika
    - [Quick overview of NCBI](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/protein_database.md): Malvika
    - [UniProt](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/UniProt.md): Ben
        - Swissprot and Trembl
        - Cross-refrences and link-outs
            - OMIM, Domains, GO, ...

2. [Study of similar sequences](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/sequence_similarity/tutorial_text.md): Marc
    - BLAST
        - BLASTP, BLASTN & PSI-BLAST
    - HMMER
    - HHPred
3. [Multiple sequence alignments](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/multiple_sequence_alignment.md): Malvika
    - Clustal omega (EMBL-EBI)
    - COBALT (NCBI)
4. Other resources
    - [Human Protein Atlas](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/HPRexercise.md): Toby
    - [Antibodypedia](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/Antibodypedia.md): Toby
    - [EMBOSS toolkits](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/EMBOSS_EBI.md): Malvika
        - [EMBOSS explorer](http://emboss.bioinformatics.nl/cgi-bin/emboss/)

**Part-2 Protein structure analysis**

*Lecture (Toby):* Secondary vs tertiary structure vs protein complexes

1. Protein Structures - Toby
    - Structure database: PDB at [RCSB](http://www.rcsb.org/pdb/home/home.do)
    - [Structure visualization](https://docs.google.com/document/d/19gtIv5fqqkEP1sJyIaCzJzMrIPKTSnT0owmk093w2C8/pub)
        - Chimera
2. Structure prediction - Malvika
    - [Secondary and Tertiary structure prediction](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/tertiary_structure_pred.md)
3. Protein-protein interaction - Michael
    - STRING and STITCH
    - Intact
    - MINT
4. [Domain databases](https://docs.google.com/document/d/1v7JM9i7yANHasTdpLFZIx_oKIx5K-t0S2o5GnDbXKD0/edit): Manjeet
    - [SMART](http://smart.embl-heidelberg.de/)
    - [Pfam](http://pfam.xfam.org/)
5. [Prediction of transmembrane helices in proteins](https://docs.google.com/document/d/1v7JM9i7yANHasTdpLFZIx_oKIx5K-t0S2o5GnDbXKD0/edit): Manjeet
    - [TMHMM](http://www.cbs.dtu.dk/services/TMHMM/)
    - [IUPRED](http://iupred.enzim.hu/) and [Anchor](http://anchor.enzim.hu/)
6. Intrinsically disordered region: Marc
    - [ELM](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/elm.md)
    - [DisProt](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/disprot.md)
7. Motif visualization:
     - [Weblogo and MEME](https://git.embl.de/sharan/protein-bioinformatics-embl-hd/blob/master/TeachingMaterials/2016/motif_visualization.md) - Malvika
     - [Jalview Alignment Viewer](https://docs.google.com/document/d/1Rd7KiqndSW3xqbW_GJc6gfU1dRkjoxR00gCi97F9VMU/pub) - Toby

### Important references
    - http://www.sciencedirect.com/science/book/9788131222973
    - http://molbiol-tools.ca/Protein_Chemistry.htm
    - http://www.ebi.ac.uk/Tools/pfa/
    - https://toolkit.tuebingen.mpg.de/
    - http://emboss.sourceforge.net/

### [Post workshop survey](https://www.surveymonkey.de/r/2GCN32Q)
