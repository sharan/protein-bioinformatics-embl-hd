# Antibodypedia

*Antibodypedia is an open-access, curated, searchable database containing annotated and scored affinity reagents to aid users in selecting antibodies tailored to specific biological and biomedical assays.*

(Same providers as the Human Protein Atlas)

https://www.antibodypedia.com

### Question
Why does Santa Cruz torture goats? 

### Examples

1. FGF13
    * Click through to the NBP2-45642 and see the validation images

2. Beta-Catenin
    * Click on the buttons – What do you get? 
	* Click on the image – What do you get? 
	* Do all the antibodies give similar ICC images?
	* Do most antibodies work for multiple methods? 

3. Look up antibodies for your favourite proteins
	* Is an antibody you use in the list? 
	* Do you think you have used the best antibody available for your purposes? 




